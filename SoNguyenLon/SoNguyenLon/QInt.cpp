#include "QInt.h"


QInt::QInt()
{
	for (int i = 0; i < 128; i++){
		arrBits[i] = 0;
	}
}


QInt::~QInt()
{
}

QInt::QInt(char *bits){
	int j = 127;
	for (int i = 0; i < 128; i++){
		SetBit(0, i);
	}
	for (int i = strlen(bits)-1; i >= 0; i--){
		SetBit(bits[i]-48, j);
		
		j--;
	}
}
void QInt::SetBit(char c, int vt){
	arrBits[vt] = c;
}

char QInt::Get_Bit(int i){
	return arrBits[i];
}

char *QInt::Get_ArrBits(){
	size_t n = sizeof(arrBits);
	char temp[128];
	strncpy(temp, arrBits, n);
	temp[n - 1] = '\0';
	return temp;
}

void QInt::SetBinary(char*bits){
	int j = 127;
	for (int i = 0; i < 128; i++){
		SetBit(0, i);
	}
	for (int i = strlen(bits) - 1; i >= 0; i--){
		SetBit(bits[i] - 48, j);

		j--;
	}
}

void QInt::SetDecimal(char *d){
	int i = 127;
	char bit;
	bool soam = 0;
	char *temp = new char[strlen(d)];
	if (d[0] == '-'){
		soam = 1;
		for (int i = 1; i < strlen(d); i++){
			temp[i - 1] = d[i];
		}
		temp[strlen(d)-1] = NULL;
	}
	else{
		strcpy(temp, d);
	}
	while (strcmp(temp, "0") != 0){
		bit = (temp[strlen(temp) - 1] - 48) % 2;
		this->SetBit(bit, i);
		temp = Div2(temp);
		i--;
	}
	if (soam){
		this->BuHai();
	}
}

void QInt::SetHex(char *h){
	char *temp = HexToBin(h);
	int n = strlen(h) * 4;
	int j = 127;
	for (int i = n - 1; i >= 0; i--){
		this->SetBit(temp[i], j);
		j--;
	}
}
void PrintOB(OverBits x)
{
	for (int i = 0; i < 256; i--)
		cout << x.OBits[i];
}

void QInt::PrintFBinary(char *s){
	fstream f;
	f.open(s, ios_base::app);
	int i = 0;
	while (arrBits[i] == 0){
		i++;
	}
	if (i == 128){
		f << "0";
		f << endl;
	}
	else{
		int k = (127 - i + 1) / 8;
		if ((127 - i + 1) % 8 != 0){
			k++;
		}
		for (i = 128 - k * 8; i < 128; i++){
			f << (int)arrBits[i];
		}
		f << endl;
	}
	f.close();
}
void QInt::PrintFHex(char *s){
	fstream f;
	f.open(s, ios_base::app);
	char *temp = BinToHex(this->arrBits);
	int i = 0;
	while (temp[i] == '0'){
		i++;
	}
	if (i == 32){
		f << "0";
	}
	else{
		while (i < 32){
			f << temp[i];
			i++;
		}
	}
	f << endl;
	f.close();
}
void QInt::PrintFDec(char *s){
	fstream f;
	f.open(s, ios_base::app);
	char *temp = BinToDec(this->arrBits);
	f << temp;
	f << endl;
	f.close();
}




void QInt::PrintBinary(){
	int i = 0;
	while (arrBits[i] == 0){
		i++;
	}
	int k = (127-i+1) / 8;
	if ((127 - i+1) % 8 != 0){
		k++;
	}
	for (i=128-k*8; i < 128; i++){
		cout << (int)arrBits[i];
	}
}
int QInt::BienDem()
{
	int i = 0;
	while (arrBits[i] == 0){
		i++;
	}
	int k = (127 - i + 1) / 8;
	if ((127 - i + 1) % 8 != 0){
		k++;
	}
	return k;
}
void QInt::PrintHex(){
	char *temp = BinToHex(this->arrBits);
	int i = 0;
	while (temp[i] == '0'){
		i++;
	}
	if (i == 32){
		cout << "0";
	}
	else{
		while (i < 32){
			cout << temp[i];
			i++;
		}
	}
}

void QInt::PrintDec(){
	char *temp = BinToDec(this->arrBits);
	cout << temp;
}

void QInt::DaoBit(){
	for (int i = 0; i < 128; i++){
		if (arrBits[i] == char(0)){
			arrBits[i] = 1;
		}
		else{
			arrBits[i] = 0;
		}
	}
}

void QInt::BuHai()
{
	int i = 127;
	while (arrBits[i] == char(0))
	{
		i--;
	}
	for (int j = 0; j < i; j++){
		if (arrBits[j] == char(0)){
			this->SetBit(1,j);
		}
		else{
			this->SetBit(0,j);
		}
	}
	arrBits[0] = 1;
}

void QInt::operator=(const QInt &x)
{
	for (int i = 0; i < 128; i++)
		arrBits[i] = x.arrBits[i];
}
void QInt::Nega_to_Posi()
{
	char k = 0;		
	for (int i = 0; i < 128; i++)
	{
		arrBits[i] += 1 + k;
		k = 0;
		if (arrBits[i]>1)
		{
			k = 1;
			arrBits[i] = arrBits[i] % 2;
		}
	}
	DaoBit();
}
QInt QInt::operator+(const QInt &b)
{
	char temp = 0;				//Bien nho
	QInt result = *this;
	for (int i = 127; i >= 0; i--){
		result.arrBits[i] = result.arrBits[i] + b.arrBits[i] + temp;
		temp = 0;
		if (result.arrBits[i]>1){
			temp = 1;
			result.arrBits[i] = result.arrBits[i] % 2;
		}
	}
	return result;

}


QInt QInt::operator-(const QInt &b)
{
	QInt temp = b;
	if (temp.Get_ArrBits()[0] == 0){//Kiem tra b>0
		temp.BuHai();
		return *this + temp;
	}	//b>0: Chuyen b thanh so am, return a + (-b)
	else{
		temp.Nega_to_Posi();			//b<0: Chuyen b sang duong, return a+b
		return *this + temp;
	}
}
void QInt::Shift_Left(int n)
{
	for (int i = 0; i < 128 - n; i++)
		arrBits[i] = arrBits[i + n];
	
	for (int i = 128 - n; i < 128; i++)
		arrBits[i] = 0;
	
}

OverBits SetOBits(char x[], char y[])
{
	OverBits Kq;
	for (int i = 0; i < 128; i++)
	{
		Kq.OBits[i] = y[i];
		Kq.OBits[i + 128] = x[i];
	}
	return Kq;
}


QInt QInt::SHR(int n){
	QInt result = *this;
	//Dich phai n bit
	for (int i = 127; i >= n; i--){
		result.arrBits[i] = result.arrBits[i - n];
	}
	//Dien 0 vao n bit ben trai
	for (int i = 0; i < n; i++){
		result.arrBits[i] = 0;
	}
	return result;
}
QInt QInt::SHL(int n){
	QInt result = *this;
	//Dich trai n bit
	for (int i = 0; i <127 - n; i++){
		result.arrBits[i] = result.arrBits[i + 1];
	}
	//Dien 0 vao n bit ben phai
	for (int i = 127 - n; i < 127; i++){
		result.arrBits[i] = 0;
	}
	return result;
}
bool QInt::isZero(){
	for (int i = 0; i < 128; i++){
		if (arrBits[i] == 1)
			return false;
	}
	return true;
}
QInt QInt::operator*(const QInt& b){		//Booth
	//Booth
	QInt A, M = b;
	char Q1 = 0;
	int k = 128;
	char At = 0, Qt = 0;//luu bit cuoi cua A, Q
	while (k > 0){
		if (Q1 == 0 && arrBits[127] == 1){
			A = A - M;
		}
		if (Q1 == 1 && arrBits[127] == 0){
			A = A + M;
		}
		//SHR [A, Q,Q1]
		At = A.arrBits[127];
		Q1 = arrBits[127];
		A = A.SHR(1);
		*this = SHR(1);
		arrBits[0] = At;
		k--;
	}
	if (!A.isZero())
	{
		//Bao loi tran bo nho
		return *this;
	}
	return *this;
}
void QInt::Shift_Right(int n)
{
	for (int i = 127; i >= n; i--)
		arrBits[i] = arrBits[i - n];
	
	
	for (int i = 0; i < n; i++)
		arrBits[i] = 0;
}

QInt QInt::operator/(const QInt& b){
	int sign = 0; //Dau ket qua
	QInt M = b;
	if (arrBits[0] != M.arrBits[0]){
		sign = 1;
	}
	if (arrBits[0] == 1)
		this->BuHai();
	if (M.arrBits[0] == 1)
		M.BuHai();
	QInt A, Q = *this;		//Khoi tao A n bit 0
	if (Q.arrBits[127] == 1){	//Q<0: A n bit 1
		A.DaoBit();
	}
	int k = 128;

	while (k > 0){

		//SHL [A, Q]
		A = A.SHL(1);
		A.arrBits[127] = Q.arrBits[0];
		Q = Q.SHL(1);
		Q.arrBits[127] = 0;
		//

		A = A - M;

		if (A.arrBits[127] == 1){
			Q.arrBits[0] = 0;
			A = A + M;
		}
		else{
			Q.arrBits[0] = 1;
		}
		k--;
	}
	if (sign == 1){
		this->BuHai();
		Q.BuHai();
		return Q;
	}

	return Q;
}

//And 
// truyen vao hai bien de so san
// tra ra bien Qint so sanh.
void  QInt::AndQint(int p1,char *bienA, char *bienB)
{
	QInt An1,An2;
	if (p1 == 10)
	{
		An1.SetDecimal(bienA);
		An2.SetDecimal(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 1&&An2.arrBits[i] == 1)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}

	}
	else if (p1==16)
	{
		An1.SetHex(bienA);
		An2.SetHex(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 1 &&An2.arrBits[i] == 1)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}
	}
	else
	{
		An1.SetBinary(bienA);
		An2.SetBinary(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 1 && An2.arrBits[i] == 1)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}
		
	}
}
//Or
// truyen vao hai bien de so sanh
// tra ra bien Qint so sanh.
void QInt::OrQint(int p1,char  *bienA, char *bienB)
{
	QInt An1, An2;
	if (p1 == 10)
	{
		An1.SetDecimal(bienA);
		An2.SetDecimal(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 0 &&An2.arrBits[i] == 0)
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
	else if (p1 == 16)
	{
		An1.SetHex(bienA);
		An2.SetHex(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 0 &&An2.arrBits[i] == 0)
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
	else
	{
		An1.SetBinary(bienA);
		An2.SetBinary(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == 0 && An2.arrBits[i] == 0)
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
}
//Xor 
// truyen vao hai bien de so sanh
// tra ra bien Qint so sanh.
void QInt::XORQint(int p1,char  *bienA, char *bienB)
{
	QInt An1, An2;
	if (p1 == 10)
	{
		An1.SetDecimal(bienA);
		An2.SetDecimal(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] ==An2.arrBits[i])
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
	else if (p1 == 16)
	{
		An1.SetHex(bienA);
		An2.SetHex(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == An2.arrBits[i])
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
	else
	{
		An1.SetBinary(bienA);
		An2.SetBinary(bienB);
		for (int i = 0; i < 128; i++)
		{
			if (An1.arrBits[i] == An2.arrBits[i])
			{
				arrBits[i] = 0;
			}
			else
			{
				arrBits[i] = 1;
			}
		}
	}
}
//Not
//truyen vao 1 bien
// truyen ra chuoi not
void QInt::NotQint(int p1, char  *bienA)
{
	QInt An;
	if (p1 == 10)
	{
		An.SetDecimal(bienA);
		for (int i = 0; i < 128; i++)
		{
			if (An.arrBits[i] == 0)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}
		
	}
	else if (p1==16)
	{
		An.SetHex(bienA);
		for (int i = 0; i < 128; i++)
		{
			if (An.arrBits[i] == 0)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}
	}
	else
	{
		for (int i = 0; i < 128; i++)
		{
			if (arrBits[i] == 0)
			{
				arrBits[i] = 1;
			}
			else
			{
				arrBits[i] = 0;
			}
		}
	}
}

//phep quay phai 1 bit
//truyen vao 1 chuoi.
// tao ra 1 chuoi quay phai 1 bit
QInt& QInt::RoLQint(int p1, char *a)
{
	if (p1 == 10)
	{
		QInt An;
		An.SetDecimal(a);
		int sl = An.BienDem() * 8;
		int i = 128-An.BienDem()*8;
		int j = 128 - An.BienDem() * 8-1;
		while (i < 128)
		{
			arrBits[j] = An.arrBits[i];
			i++;
			j++;
		}
		arrBits[j] = An.arrBits[128 - An.BienDem() * 8];
		char *Abien1 = new char[sl];
		int ian = 0;
		int idem = 128 - An.BienDem() * 8;
		while (ian < sl)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		Abien1[sl] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
	else if (p1 == 16)
	{
		QInt An;
		An.SetHex(a);
		int sl = An.BienDem() * 8;
		int i = 128 - An.BienDem() * 8;
		int j = 128 - An.BienDem() * 8 - 1;
		while (i < 128)
		{
			arrBits[j] = An.arrBits[i];
			i++;
			j++;
		}
		arrBits[j] = An.arrBits[128 - An.BienDem() * 8];
		char *Abien1=new char[sl];
		int ian = 0;
		int idem = 128 - An.BienDem() * 8;
		while (ian < sl)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		An.~QInt();
		Abien1[sl] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
	else
	{
		QInt An(a);
		int sl = An.BienDem() * 8;
		int i = 128 - An.BienDem() * 8;
		int j = 128 - An.BienDem() * 8 - 1;
		while (i < 128)
		{
			arrBits[j] = An.arrBits[i];
			i++;
			j++;
		}
		arrBits[j] = An.arrBits[128 - An.BienDem() * 8];
		char *Abien1 = new char[sl];
		int ian = 0;
		int idem = 128 - An.BienDem() * 8;
		while (ian < sl)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		Abien1[sl] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
}

//phep quay trai 1 bit
//truyen vao 1 chuoi.
// tao ra 1 chuoi quay phai 1 bit
QInt& QInt::RoRQint(int p1, char *a)
{

	if (p1==10)
	{
		QInt RolAn;
		RolAn.SetDecimal(a);
		int slrol = RolAn.BienDem() * 8;
		int irol = 128 - slrol-1;
		int jrol = 128 - slrol-1;
		arrBits[jrol] = RolAn.arrBits[127];
		jrol++;
		while (irol < 127)
		{
			arrBits[jrol] = RolAn.arrBits[irol];
			jrol++;
			irol++;
		}
		char *Abien1 = new char[slrol];
		int ian = 0;
		int idem = 128 - RolAn.BienDem() * 8;
		while (ian < slrol)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		Abien1[slrol] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
	else if (p1 == 16)
	{
		QInt RolAn;
		RolAn.SetHex(a);
		RolAn.PrintBinary();
		int slrol = RolAn.BienDem() * 8;
		int irol = 128 - slrol - 1;
		int jrol = 128 - slrol - 1;
		arrBits[jrol] = RolAn.arrBits[127];
		jrol++;
		while (irol < 127)
		{
			arrBits[jrol] = RolAn.arrBits[irol];
			jrol++;
			irol++;
		}
		char *Abien1 = new char[slrol];
		int ian = 0;
		int idem = 128 - RolAn.BienDem() * 8;
		while (ian < slrol)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		Abien1[slrol] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
	else
	{
		QInt RolAn(a);
		int slrol = RolAn.BienDem() * 8;
		int irol = 128 - slrol;
		int jrol = 128 - slrol;
		arrBits[jrol] = RolAn.arrBits[127];
		jrol++;
		while (jrol < 128)
		{
			arrBits[jrol] = RolAn.arrBits[irol];
			jrol++;
			irol++;
		}
		char *Abien1 = new char[slrol];
		int ian = 0;
		int idem = 128 - RolAn.BienDem() * 8-1;
		while (ian < slrol)
		{
			if (arrBits[idem] == '\0')
			{
				Abien1[ian] = '0';
			}
			else
			{
				Abien1[ian] = '1';
			}
			ian++;
			idem++;
		}
		Abien1[slrol] = '\0';
		QInt AnBien(Abien1);
		return AnBien;
	}
}

//dich trai 
QInt QInt::operator<<(int sld)
{
	QInt DF;
	for (int i = 0; i < 128-sld; i++)
	{
		DF.SetBit(arrBits[i + sld], i);
	}
	for (int i = 128-sld; i < 128; i++)
	{
		DF.SetBit(0,i);
	}
	for (int i = 0; i < 128; i++)
	{
		arrBits[i] = DF.arrBits[i];
	}
	return *this;

}
QInt QInt::operator>>(int sld)
{
	QInt DF;
	for (int i=sld; i< 128 ; i++)
	{
		DF.SetBit(arrBits[i - sld], i);
	}
	for (int i = 0; i < sld; i++)
	{
     	DF.SetBit(0, i);
	}
	for (int i = 0; i < 128; i++)
	{
		arrBits[i] = DF.arrBits[i];
	}
	return *this;
}