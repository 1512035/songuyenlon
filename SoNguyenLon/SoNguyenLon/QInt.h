#pragma once
#include <iostream>
#include <stdlib.h>
#include "Convert.h"
#include <fstream>
#pragma warning(disable:4996)
using namespace std;
struct OverBits
{
	char OBits[256];
};

class QInt
{
private:
	char arrBits[128];
public:
	QInt();
	~QInt();
	QInt(char*); //Nhap bit kieu nhi phan
	char Get_Bit(int);
	char *Get_ArrBits();
	void SetBit(char c, int vt); //Set bit tai vi tri vt
	void SetBinary(char*);
	void SetDecimal(char *);//Gan gia tri QInt voi gia tri Decimal
	void SetHex(char*);//Gan gia tri QInt voi gia tri hexadecimal 
	void PrintBinary();//Xuat QInt dang nhi phan
	void PrintHex();
	void PrintDec();
	void DaoBit();
	void BuHai();
	void PrintFBinary(char *);
	void PrintFHex(char *);
	void PrintFDec(char *);
	//Phep so hoc
	void operator=(const QInt &x);
	QInt operator+(const QInt &x);
	QInt operator-(const QInt &x);
	QInt operator*(const QInt &x);
	QInt operator/(const QInt &x);
	//Chuyen am thanh duong
	void Nega_to_Posi();
	//Phep dich
	QInt SHR(int n);
	bool isZero();
	QInt SHL(int n);
	void Shift_Right(int n);
	void Shift_Left(int n);

	// phep so sanh logic
	void AndQint(int p1,char *bienA,char *bienB);
	void OrQint(int p1, char  *bienA, char *bienB);
	void XORQint(int p1, char  *bienA, char *bienB);
	void NotQint(int p1, char  *bienA);
	//Phep quay
	QInt& RoRQint(int p1, char *a);
	QInt& RoLQint(int p1,char *a);
	QInt operator>>(int sld);
	QInt operator<<(int sld);
	//bien dem;
	int BienDem();

};
void PrintOB(OverBits x);
