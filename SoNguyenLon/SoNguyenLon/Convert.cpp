#include "Convert.h"
#include "QInt.h"

char * BinToHex(char* a){
	char *hex = new char[34];
	char *temp = a;
	char c[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	for (int i = 0; i < 32; i++)
	{							//128 bit bieu dien bang 32 ky tu hex ko dau + 2 ky tu dau cuoi
		int k = 0;
		for (int j = 0; j < 4; j++){				//Chuyen 4 bit thanh 1 hex
			k += a[i * 4 + j] * pow(2, 4-j-1);
		}
		hex[i] = c[k];
	}

	hex[32] = '\0';

	return hex;
}
char * HexToBin(char *a){
	int n = strlen(a) * 4;
	char *temp = new char[n];
	for (int i = 0; i < strlen(a); i++){
		if (a[i] == '0'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 0;
		}
		if (a[i] == '1'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == '2'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == '3'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == '4'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == '5'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == '6'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == '7'){
			temp[i * 4 + 0] = 0;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == '8'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 0;
		}
		if (a[i] == '9'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == 'A'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == 'B'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 0;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == 'C'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == 'D'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 0;
			temp[i * 4 + 3] = 1;
		}

		if (a[i] == 'E'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 2] = 1;
			temp[i * 4 + 3] = 0;
		}

		if (a[i] == 'F'){
			temp[i * 4 + 0] = 1;
			temp[i * 4 + 1] = 1;
			temp[i * 4 + 3] = 1;
			temp[i * 4 + 2] = 1;
		}
	}
	return temp;
}
char* Div2(char *t){
	char *s = new char[strlen(t)];
	int du = 0;
	char *temp = new char[2];
	char *ghi = new char;
	int n;
	int j = 0;//con tro trong chuoi s
	int i = 0;// con tro trong chuoi t
	char *tam = new char;
	if (t[0] - 48 == 1){
		temp[0] = t[0];
		temp[1] = t[1];
		i += 2;
	}
	else{
		temp[0] = t[0];
		i++;
	}

	n = atoi(temp);
	du = n % 2;
	n = n / 2;
	itoa(n, ghi, 10);
	s[j] = ghi[0];
	j++;

	while (i < strlen(t)){
		itoa(du, tam, 10);
		temp[0] = tam[0];
		temp[1] = t[i];

		n = atoi(temp);
		du = n % 2;
		n = n / 2;
		itoa(n, ghi, 10);
		s[j] = ghi[0];
		j++;
		i++;
	}
	s[j] = NULL;
	return s;
}
void swapPointer(char** a, char** b)
{
	char* temp = *a;
	*a = *b;
	*b = temp;
}
char* Cong(char* a, char* b)
{
	
	if (strlen(a) < strlen(b))
	{
		swapPointer(&a, &b);
	}

	size_t a_len = strlen(a), b_len = strlen(b);
	char* result = new char[a_len + 2];
	memset(result, '0', a_len);
	bool remember = false;

	for (int i = 0; i < b_len; i++)
	{
		int temp = *(b + b_len - i - 1) - '0' + *(a + a_len - i - 1) - '0';

		if (remember)
			temp++;

		if (temp > 9)
			remember = true;
		else
			remember = false;

		temp = temp % 10;

		*(result + a_len - i) = temp + '0';
	}

	for (int i = 0; i < a_len - b_len; i++)
	{
		int temp = *(a + a_len - b_len - i - 1) - '0';

		if (remember)
			temp++;
		if (temp > 9)
			remember = true;
		else
			remember = false;

		temp = temp % 10;

		*(result + a_len - b_len - i) = temp + '0';
	}

	*(result + a_len + 1) = '\0';

	if (remember)
	{
		*(result) = '1';
	}
	else
	{
		for (int i = 0; i <= a_len; i++)
			*(result + i) = *(result + i + 1);
	}

	return result;
}
char* Tru(char* a, char* b)
{
	bool sign = false, remember = false;

	if (strlen(a) < strlen(b))
	{
		sign = true;
		swapPointer(&a, &b);
	}
	else if (strlen(a) == strlen(b))
	{
		for (int i = strlen(a) - 1; i >= 0; i--)
		{
			if (*(a + strlen(a) - i - 1) < *(b + strlen(b) - i+1))
			{
				swapPointer(&a, &b);
				sign = true;
			}
		}
	}
	size_t a_len = strlen(a);
	size_t b_len = strlen(b);
	char* result = new char[a_len + 2];
	memset(result, '0', a_len + 2);

	for (int i = 0; i < b_len; i++)
	{
		int temp = *(a + a_len - i - 1) - *(b + b_len - i - 1);

		if (remember)
			temp--;
		if (temp < 0)
		{
			temp += 10;
			remember = true;
		}
		else
			remember = false;

		*(result + a_len - i) = temp + '0';
	}

	for (int i = 0; i < a_len - b_len; i++)
	{
		int temp = *(a + a_len - b_len - i - 1) - '0';

		if (remember == true)
			temp--;
		if (temp < 0)
		{
			temp += 10;
			remember = true;
		}
		else
			remember = false;

		*(result + a_len - b_len - i) = temp + '0';
	}

	while (*(result) == '0')
	{
		if (*(result + 1) != '0' && sign == true)
		{
			*(result) = '-';
			*(result + a_len + 1) = '\0';
			break;
		}

		for (int i = 0; i < a_len; i++)
		{
			*(result + i) = *(result + i + 1);
		}
		*(result + a_len) = '\0';
	}

	return result;
}
char * Nhan(char *a, char *b){
	char *tong = new char[330];
	tong = "0";
	char *temp = new char[330];
	strcpy(temp, b);
	//a*b=a+a+a+a+a+....(b lan)
	while (temp[0]!=0)
	{
		tong = Cong(tong, a);
		temp = Tru(temp, "1");
	}
	return tong;
}
char * LuyThua(char *a, char *b){
	
	char *tich = new char[330];
	tich = "1";
	char *temp = new char[330];
	strcpy(temp, b);

	if (temp[0] == '0'){
		return "1";
	}
	//a^n =a*a*a*a*a....(n lan)
	while (temp[0] != 0)
	{
		tich = Nhan(tich, a);
		temp = Tru(temp, "1");
	}
	return tich;
}
int SosanhChuoi(char*a, char*b){
	if (strlen(a) > strlen(b)){
		return 1;
	}
	if (strlen(a) < strlen(b)){
		return -1;
	}
	int i = 0;
	while (a[i] == b[i]){
		i++;
	}
	if (a[i]>b[i]){
		return 1;
	}
	else{
		return -1;
	}
	if (i== strlen(a)){
		return 0;
	}
}
char *Chia(char*a, char*b){
	char *temp=new char[128];
	strcpy(temp, a);
	char *kq = new char[128];
	kq = "0";
	while (SosanhChuoi(temp,b)>=0){
		temp = Tru(temp, b);
		kq = Cong(kq, "1");
	}
	return kq;
}



char* BinToDec(char *a){
	char *dau = new char[321];
	bool soam = false;
	if (a[0] == 1){
		dau = LuyThua("2", "127");
		soam = true;
	}
		char *tong = new char[321];
		tong = "0";
		char *temp = new char[321];
		char* somu = new char[130];

		for (int i = 128 - 1; i > 0; i--){
			if (a[i] != 0){
				itoa(128 - i - 1, somu, 10);
				temp = LuyThua("2", somu);
				tong = Cong(tong, temp);
			}
			else{
				temp = "0";
				
			}
			
		}

		delete[]somu;
		if (soam){
			tong = Tru(tong, dau);
		}
		return tong;
}

//ham chuyen key cua phep toan lo_ic va quay
int ChuyenKey(char *temp2)
{
	int KeyLogic=0;
	if (!strcmp(temp2, "ror"))
	{
		KeyLogic = 1;
	}
	if (!strcmp(temp2, "rol"))
	{
		KeyLogic = 2;
	}
	if (!strcmp(temp2, "&"))
	{
		KeyLogic = 3;
	}
	if (!strcmp(temp2, "|"))
	{
		KeyLogic = 4;
	}
	if (!strcmp(temp2, "^"))
	{
		KeyLogic = 5;
	}
	if (!strcmp(temp2, "~"))
	{
		KeyLogic = 6;
	}
	return KeyLogic;
}

//Ham Chuyen key phep toan so hoc va dich bit
int CoverKey(char* temp3)
{
	if (!strcmp(temp3, "+"))
		return 1;
	if (!strcmp(temp3, "-"))
		return 2;
	if (!strcmp(temp3, "*"))
		return 3;
	if (!strcmp(temp3, "/"))
		return 4;
	if (!strcmp(temp3, ">>"))
		return 5;
	if (!strcmp(temp3, "<<"))
		return 6;
}

//char to char
