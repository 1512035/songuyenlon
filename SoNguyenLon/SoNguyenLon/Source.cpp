﻿#include "Convert.h"
#include "QInt.h"
#include <vector>



int main(int argc, char *argv[])

{
	int keyQint;
	vector<char*> listQ;

	fstream fDoc;
	fstream fGhi;
	fDoc.open(argv[1], ios_base::in);
	fGhi.open(argv[2], ios_base::out);
	fGhi.close();


	while (!fDoc.eof())
	{
		char *temp = new char[300];
		fDoc.getline(temp, 255);
		listQ.push_back(temp);
	}
	int p1, p2;
	for (int i = 0; i < listQ.size(); i++)
	{
		char *temp1, *temp2, *temp3, *temp4;
		temp1 = strtok(listQ[i], " ");
		temp2 = strtok(NULL, " ");
		temp3 = strtok(NULL, " ");
		temp4 = strtok(NULL, " ");
		p1 = atoi(temp1);
		p2 = atoi(temp2);
		if (p2 == 10 || p2 == 2 || p2 == 16 )
		{
			keyQint = 1;
			if ((int)temp3[0] == 62 || (int)temp3[0] == 60)
			{
				keyQint = 3;
			}
		}
		else if (p2 == 0)
		{

			keyQint = 2;
			int keytemp = ChuyenKey(temp2);
			if (keytemp == 0)
			{
				keyQint = 3;
			}
		}
		else
		{
			keyQint = 3;
		}
		switch (keyQint)
		{
		case 1:// dieu kien cua binh kute
		{// chuyen doi so
				  switch (p1 - p2)
				  {
				  case -8:
				  {// 2->10
							  QInt a(temp3);
							  a.PrintFDec(argv[2]);
							 
							  break;
				  }
				  case 8:
				  {// 10->2
							 QInt a;
							 a.SetDecimal(temp3);
							 a.PrintFBinary(argv[2]);
							 
							 break;
				  }
				  case -14:
				  {// 2->16
							   QInt a(temp3);
							   a.PrintFHex(argv[2]);
							 
							   break;
				  }
				  case 14:
				  {//16->2
							  QInt a;
							  a.SetHex(temp3);
							  a.PrintFBinary(argv[2]);
							 
							  break;
				  }
				  case -6:
				  {//10->16
							  QInt a;
							  a.SetDecimal(temp3);
							  a.PrintFHex(argv[2]);
							 
							  break;
				  }
				  case 6:
				  {//16->10
							 QInt a;
							 a.SetHex(temp3);
							 a.PrintFDec(argv[2]);
							 
							 break;
				  }
				  default:
					  break;
				  }
		}; break;
		///////////////////
		///////////////////
		case 2:// dieu kien an kute
		{//phep toan logic va quay 
				   int KeyLogic = ChuyenKey(temp2);
				   switch (KeyLogic)
				   {
				   case 1:// phep quay phai ror
				   {
							  QInt An;
							  An.RoRQint(p1, temp3);
							  if (p1 == 10)
							  {
								  An.PrintFDec(argv[2]);
							  }
							  else if (p1==16)
							  {
								  An.PrintFHex(argv[2]);
							  }
							 else
							 {
								 An.PrintFBinary(argv[2]);
							 }
							 
							  An.~QInt();
				   }; break;
				   case 2:// quay trai rol
				   {
							  QInt Anrolmain;
							  Anrolmain.RoLQint(p1, temp3);
							  if (p1 == 10)
							  {
								  Anrolmain.PrintFDec(argv[2]);
							  }
							  else if (p1 == 16)
							  {
								  Anrolmain.PrintFHex(argv[2]);
							  }
							  else
							  {
								  Anrolmain.PrintFBinary(argv[2]);
							  }
							  Anrolmain.~QInt();
				   }; break;
				   case 3:// phep logic And &
				   {
							  QInt An;
							  An.AndQint(p1, temp3, temp4);
							  if (p1 == 10)
							  {
								  An.PrintFDec(argv[2]);
							  }
							  else if (p1 == 16)
							  {
								  An.PrintFHex(argv[2]);
							  }
							  else
							  {
								  An.PrintFBinary(argv[2]);
							  }
							  cout << endl;
				   }; break;
				   case 4://phep OR
				   {
							 QInt An;
							  An.OrQint(p1, temp3, temp4);
							  if (p1 == 10)
							 {
								  An.PrintFDec(argv[2]);
							 }
							  else if (p1 == 16)
							 {
								  An.PrintFHex(argv[2]);
							 }
							 else
							 {
								 An.PrintFBinary(argv[2]);
							 }
							 cout << endl;

				   }; break;
				   case 5:// phep XOR
				   {
							  QInt An;
							  An.XORQint(p1, temp3, temp4);
							  if (p1 == 10)
							  {
								  An.PrintFDec(argv[2]);
							  }
							  else if (p1 == 16)
							  {
								  An.PrintFHex(argv[2]);
							  }
							  else
							  {
								  An.PrintFBinary(argv[2]);
							  }
							  cout << endl;

				   }; break;
				   case 6://phep Not
				   {
							  QInt An;
							  An.NotQint(p1, temp3);
							  if (p1 == 10)
							  {
								  An.PrintFDec(argv[2]);
							  }
							  else if (p1 == 16)
							  {
								  An.PrintFHex(argv[2]);
							  }
							  else
							  {
								  An.PrintFBinary(argv[2]);
							  }
							  cout << endl;

				   }; break;
				   }
		}; break;
		///////////////////
		///////////////////
		case 3:// dieu kien cua danh kute
		{
				   QInt a, b;
				   switch (p1)
				   {
				   case 2:{
							  a.SetBinary(temp2);
							  b.SetBinary(temp4);
							  break;
				   }
				   case 10:{
							   a.SetDecimal(temp2);
							   b.SetDecimal(temp4);
							   break;
				   }
				   case 16:{
							   a.SetHex(temp2);
							   b.SetHex(temp4);
							   break;
				   }
				   default:
					   break;
				   }



					   int k = CoverKey(temp3);
				   QInt kq;
				   switch (k)
				   {
				   case 1:{
							  kq = a + b;
							  break;
				   }
				   case 2:{
							  kq = a - b;
							  break;
				   }
				   case 3:{
							  kq = a * b;
							  break;
				   }
				   case 4:{
							  kq = a / b;
							  break;
				   }
				   case 5:{
							  kq = a;
							  int sl = atoi(temp4);
							  if (p1 == 10)
							  {
								  kq.SetDecimal(temp2);
							  }
							  else if (p1 == 16)
							  {
								  kq.SetHex(temp2);
							  }
							  else
							  {
								  kq.SetBinary(temp2);
							  }
							  kq >>(sl);
							  break;
				   }
				   case 6:{
							  kq = a;
							  int sl = atoi(temp4);
							  if (p1 == 10)
							  {
								  kq.SetDecimal(temp2);
							  }
							  else if (p1 == 16)
							  {
								  kq.SetHex(temp2);
							  }
							  else
							  {
								  kq.SetBinary(temp2);
							  }
							  kq <<(sl);
							  break;
				   }
				   default:
					   break;
				   }


				   switch (p1)
				   {
				   case 2:{
							  kq.PrintFBinary(argv[2]);
							  
							  break;
				   }
				   case 10:{
							   kq.PrintFDec(argv[2]);
							   
							   break;
				   }
				   case 16:{
							   kq.PrintFHex(argv[2]);
							   
							   break;
				   }
				   default:
					   break;
				   }


		}; break;
		//ad
		}
	}

	fDoc.close();
	cout << "Da thuc hien xong!";
	return 0;
}




